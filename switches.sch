EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RP2040:RP2040 IC1
U 1 1 62C1DFA2
P 4175 2325
F 0 "IC1" H 6219 1771 50  0000 L CNN
F 1 "RP2040" H 6219 1680 50  0000 L CNN
F 2 "RP2040:QFN40P700X700X90-57N-D" H 6025 3025 50  0001 L CNN
F 3 "https://datasheets.raspberrypi.org/rp2040/rp2040-datasheet.pdf" H 6025 2925 50  0001 L CNN
F 4 "Microcontroller in QFN Package" H 6025 2825 50  0001 L CNN "Description"
F 5 "0.9" H 6025 2725 50  0001 L CNN "Height"
F 6 "RASPBERRY-PI" H 6025 2625 50  0001 L CNN "Manufacturer_Name"
F 7 "RP2040" H 6025 2525 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 6025 2425 50  0001 L CNN "Mouser Part Number"
F 9 "" H 6025 2325 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6025 2225 50  0001 L CNN "Arrow Part Number"
F 11 "" H 6025 2125 50  0001 L CNN "Arrow Price/Stock"
	1    4175 2325
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R10
U 1 1 62C1DFA8
P 5800 3725
F 0 "R10" H 5859 3771 50  0000 L CNN
F 1 "27.4" H 5859 3680 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 5800 3725 50  0001 C CNN
F 3 "~" H 5800 3725 50  0001 C CNN
	1    5800 3725
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R9
U 1 1 62C1DFAE
P 5800 3625
F 0 "R9" H 5859 3671 50  0000 L CNN
F 1 "27.4" H 5859 3580 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 5800 3625 50  0001 C CNN
F 3 "~" H 5800 3625 50  0001 C CNN
	1    5800 3625
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5700 3725 5075 3725
Wire Wire Line
	5075 3625 5700 3625
$Comp
L Memory_Flash:W25Q32JVSS U1
U 1 1 62C1E839
P 6975 2925
F 0 "U1" H 7875 3350 50  0000 C CNN
F 1 "W25Q32JVSS" H 7675 3250 50  0000 C CNN
F 2 "Package_SO:SOIC-8_5.23x5.23mm_P1.27mm" H 6975 2925 50  0001 C CNN
F 3 "http://www.winbond.com/resource-files/w25q32jv%20revg%2003272018%20plus.pdf" H 6975 2925 50  0001 C CNN
	1    6975 2925
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5075 2825 6475 2825
Wire Wire Line
	5075 2925 6150 2925
Wire Wire Line
	6150 2925 6150 3025
Wire Wire Line
	6150 3025 6475 3025
Wire Wire Line
	5075 3025 5950 3025
Wire Wire Line
	5950 3025 5950 2725
Wire Wire Line
	5950 2725 6475 2725
Wire Wire Line
	5075 3225 6475 3225
Wire Wire Line
	6475 3225 6475 3125
Wire Wire Line
	5075 3125 6325 3125
Wire Wire Line
	6325 3125 6325 3450
Wire Wire Line
	6325 3450 7525 3450
Wire Wire Line
	7525 3450 7525 3025
Wire Wire Line
	7525 3025 7475 3025
Wire Wire Line
	7475 2825 7475 2425
Wire Wire Line
	7475 2025 5850 2025
Wire Wire Line
	5850 2025 5850 2725
Wire Wire Line
	5850 2725 5075 2725
$Comp
L Device:R_Small R6
U 1 1 62C26343
P 7250 2425
F 0 "R6" V 7054 2425 50  0000 C CNN
F 1 "10k" V 7145 2425 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 7250 2425 50  0001 C CNN
F 3 "~" H 7250 2425 50  0001 C CNN
	1    7250 2425
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 62C26F4D
P 7675 2825
F 0 "R7" V 7479 2825 50  0000 C CNN
F 1 "10k" V 7570 2825 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 7675 2825 50  0001 C CNN
F 3 "~" H 7675 2825 50  0001 C CNN
	1    7675 2825
	0    1    1    0   
$EndComp
Wire Wire Line
	7350 2425 7475 2425
Connection ~ 7475 2425
Wire Wire Line
	7475 2425 7475 2025
Wire Wire Line
	7150 2425 6975 2425
Wire Wire Line
	6975 2425 6975 2525
Wire Wire Line
	7575 2825 7475 2825
Connection ~ 7475 2825
$Comp
L Connector_Generic:Conn_01x02 J7
U 1 1 62C282A8
P 8050 2825
F 0 "J7" H 8130 2817 50  0000 L CNN
F 1 "Conn_01x02" H 8130 2726 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8050 2825 50  0001 C CNN
F 3 "~" H 8050 2825 50  0001 C CNN
	1    8050 2825
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 2825 7775 2825
$Comp
L power:GND #PWR012
U 1 1 62C2990D
P 7850 3000
F 0 "#PWR012" H 7850 2750 50  0001 C CNN
F 1 "GND" H 7855 2827 50  0000 C CNN
F 2 "" H 7850 3000 50  0001 C CNN
F 3 "" H 7850 3000 50  0001 C CNN
	1    7850 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 62C2A2B3
P 6975 3500
F 0 "#PWR017" H 6975 3250 50  0001 C CNN
F 1 "GND" H 6980 3327 50  0000 C CNN
F 2 "" H 6975 3500 50  0001 C CNN
F 3 "" H 6975 3500 50  0001 C CNN
	1    6975 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 62C2AA57
P 6625 2425
F 0 "#PWR010" H 6625 2175 50  0001 C CNN
F 1 "GND" H 6630 2252 50  0000 C CNN
F 2 "" H 6625 2425 50  0001 C CNN
F 3 "" H 6625 2425 50  0001 C CNN
	1    6625 2425
	0    1    1    0   
$EndComp
Wire Wire Line
	6975 3500 6975 3325
$Comp
L Device:C_Small C3
U 1 1 62C2B679
P 6800 2425
F 0 "C3" V 6571 2425 50  0000 C CNN
F 1 "0.1u" V 6662 2425 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6800 2425 50  0001 C CNN
F 3 "~" H 6800 2425 50  0001 C CNN
	1    6800 2425
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 2425 6975 2425
Connection ~ 6975 2425
Wire Wire Line
	6700 2425 6625 2425
$Comp
L power:+3.3V #PWR09
U 1 1 62C2CC43
P 6975 2350
F 0 "#PWR09" H 6975 2200 50  0001 C CNN
F 1 "+3.3V" H 6990 2523 50  0000 C CNN
F 2 "" H 6975 2350 50  0001 C CNN
F 3 "" H 6975 2350 50  0001 C CNN
	1    6975 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6975 2350 6975 2425
Wire Wire Line
	7850 3000 7850 2925
$Comp
L power:GND #PWR05
U 1 1 62C31920
P 2925 1925
F 0 "#PWR05" H 2925 1675 50  0001 C CNN
F 1 "GND" H 2930 1752 50  0000 C CNN
F 2 "" H 2925 1925 50  0001 C CNN
F 3 "" H 2925 1925 50  0001 C CNN
	1    2925 1925
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C1
U 1 1 62C31926
P 3100 1925
F 0 "C1" V 2871 1925 50  0000 C CNN
F 1 "0.1u" V 2962 1925 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3100 1925 50  0001 C CNN
F 3 "~" H 3100 1925 50  0001 C CNN
	1    3100 1925
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 1925 3275 1925
Connection ~ 3275 1925
Wire Wire Line
	3000 1925 2925 1925
$Comp
L power:+3.3V #PWR04
U 1 1 62C3192F
P 3275 1850
F 0 "#PWR04" H 3275 1700 50  0001 C CNN
F 1 "+3.3V" H 3290 2023 50  0000 C CNN
F 2 "" H 3275 1850 50  0001 C CNN
F 3 "" H 3275 1850 50  0001 C CNN
	1    3275 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3275 1850 3275 1925
Wire Wire Line
	3275 1925 3275 2325
Wire Wire Line
	4175 2225 4175 2325
$Comp
L power:GND #PWR08
U 1 1 62C33FDB
P 4525 2225
F 0 "#PWR08" H 4525 1975 50  0001 C CNN
F 1 "GND" H 4530 2052 50  0000 C CNN
F 2 "" H 4525 2225 50  0001 C CNN
F 3 "" H 4525 2225 50  0001 C CNN
	1    4525 2225
	0    -1   1    0   
$EndComp
$Comp
L Device:C_Small C2
U 1 1 62C33FE1
P 4350 2225
F 0 "C2" V 4121 2225 50  0000 C CNN
F 1 "0.1u" V 4212 2225 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 4350 2225 50  0001 C CNN
F 3 "~" H 4350 2225 50  0001 C CNN
	1    4350 2225
	0    -1   1    0   
$EndComp
Wire Wire Line
	4250 2225 4225 2225
Connection ~ 4175 2225
Wire Wire Line
	4450 2225 4525 2225
$Comp
L power:+3.3V #PWR06
U 1 1 62C33FEA
P 4175 2150
F 0 "#PWR06" H 4175 2000 50  0001 C CNN
F 1 "+3.3V" H 4190 2323 50  0000 C CNN
F 2 "" H 4175 2150 50  0001 C CNN
F 3 "" H 4175 2150 50  0001 C CNN
	1    4175 2150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4175 2150 4175 2225
$Comp
L power:GND #PWR014
U 1 1 62C3B2A6
P 925 3325
F 0 "#PWR014" H 925 3075 50  0001 C CNN
F 1 "GND" H 930 3152 50  0000 C CNN
F 2 "" H 925 3325 50  0001 C CNN
F 3 "" H 925 3325 50  0001 C CNN
	1    925  3325
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C6
U 1 1 62C3B2AC
P 1100 3325
F 0 "C6" V 871 3325 50  0000 C CNN
F 1 "0.1u" V 962 3325 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 1100 3325 50  0001 C CNN
F 3 "~" H 1100 3325 50  0001 C CNN
	1    1100 3325
	0    1    1    0   
$EndComp
Wire Wire Line
	1200 3325 1275 3325
Wire Wire Line
	1000 3325 925  3325
$Comp
L power:+3.3V #PWR013
U 1 1 62C3B2B5
P 1275 3250
F 0 "#PWR013" H 1275 3100 50  0001 C CNN
F 1 "+3.3V" H 1290 3423 50  0000 C CNN
F 2 "" H 1275 3250 50  0001 C CNN
F 3 "" H 1275 3250 50  0001 C CNN
	1    1275 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1275 3250 1275 3325
Wire Wire Line
	1275 3325 2075 3325
Connection ~ 1275 3325
$Comp
L power:GND #PWR035
U 1 1 62C3FC3C
P 2925 4875
F 0 "#PWR035" H 2925 4625 50  0001 C CNN
F 1 "GND" H 2930 4702 50  0000 C CNN
F 2 "" H 2925 4875 50  0001 C CNN
F 3 "" H 2925 4875 50  0001 C CNN
	1    2925 4875
	0    1    -1   0   
$EndComp
$Comp
L Device:C_Small C18
U 1 1 62C3FC42
P 3100 4875
F 0 "C18" V 2871 4875 50  0000 C CNN
F 1 "0.1u" V 2962 4875 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3100 4875 50  0001 C CNN
F 3 "~" H 3100 4875 50  0001 C CNN
	1    3100 4875
	0    1    -1   0   
$EndComp
Wire Wire Line
	3200 4875 3275 4875
Connection ~ 3275 4875
Wire Wire Line
	3000 4875 2925 4875
$Comp
L power:+3.3V #PWR037
U 1 1 62C3FC4B
P 3275 4950
F 0 "#PWR037" H 3275 4800 50  0001 C CNN
F 1 "+3.3V" H 3290 5123 50  0000 C CNN
F 2 "" H 3275 4950 50  0001 C CNN
F 3 "" H 3275 4950 50  0001 C CNN
	1    3275 4950
	1    0    0    1   
$EndComp
Wire Wire Line
	3275 4950 3275 4875
Wire Wire Line
	3275 4325 3275 4875
Wire Wire Line
	4175 4425 4175 4325
$Comp
L power:GND #PWR030
U 1 1 62C43D52
P 4525 4425
F 0 "#PWR030" H 4525 4175 50  0001 C CNN
F 1 "GND" H 4530 4252 50  0000 C CNN
F 2 "" H 4525 4425 50  0001 C CNN
F 3 "" H 4525 4425 50  0001 C CNN
	1    4525 4425
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C15
U 1 1 62C43D58
P 4350 4425
F 0 "C15" V 4121 4425 50  0000 C CNN
F 1 "0.1u" V 4212 4425 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 4350 4425 50  0001 C CNN
F 3 "~" H 4350 4425 50  0001 C CNN
	1    4350 4425
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4250 4425 4175 4425
Connection ~ 4175 4425
Wire Wire Line
	4450 4425 4525 4425
$Comp
L power:+3.3V #PWR032
U 1 1 62C43D61
P 4175 4500
F 0 "#PWR032" H 4175 4350 50  0001 C CNN
F 1 "+3.3V" H 4190 4673 50  0000 C CNN
F 2 "" H 4175 4500 50  0001 C CNN
F 3 "" H 4175 4500 50  0001 C CNN
	1    4175 4500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4175 4500 4175 4425
Wire Wire Line
	6325 3625 6325 3550
$Comp
L power:+3.3V #PWR019
U 1 1 62C46BEE
P 6325 3625
F 0 "#PWR019" H 6325 3475 50  0001 C CNN
F 1 "+3.3V" H 6340 3798 50  0000 C CNN
F 2 "" H 6325 3625 50  0001 C CNN
F 3 "" H 6325 3625 50  0001 C CNN
	1    6325 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 3550 6675 3550
Wire Wire Line
	6400 3550 6325 3550
$Comp
L Device:C_Small C9
U 1 1 62C46BE5
P 6500 3550
F 0 "C9" V 6271 3550 50  0000 C CNN
F 1 "0.1u" V 6362 3550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6500 3550 50  0001 C CNN
F 3 "~" H 6500 3550 50  0001 C CNN
	1    6500 3550
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR018
U 1 1 62C46BDF
P 6675 3550
F 0 "#PWR018" H 6675 3300 50  0001 C CNN
F 1 "GND" H 6680 3377 50  0000 C CNN
F 2 "" H 6675 3550 50  0001 C CNN
F 3 "" H 6675 3550 50  0001 C CNN
	1    6675 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6325 3550 6275 3550
Wire Wire Line
	6275 3550 6275 3425
Wire Wire Line
	6275 3425 5075 3425
Connection ~ 6325 3550
Wire Wire Line
	6325 3975 6325 3875
$Comp
L power:GND #PWR021
U 1 1 62C55C97
P 6675 3975
F 0 "#PWR021" H 6675 3725 50  0001 C CNN
F 1 "GND" H 6680 3802 50  0000 C CNN
F 2 "" H 6675 3975 50  0001 C CNN
F 3 "" H 6675 3975 50  0001 C CNN
	1    6675 3975
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C11
U 1 1 62C55C9D
P 6500 3975
F 0 "C11" V 6271 3975 50  0000 C CNN
F 1 "0.1u" V 6362 3975 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6500 3975 50  0001 C CNN
F 3 "~" H 6500 3975 50  0001 C CNN
	1    6500 3975
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6400 3975 6325 3975
Connection ~ 6325 3975
Wire Wire Line
	6600 3975 6675 3975
$Comp
L power:+3.3V #PWR023
U 1 1 62C55CA6
P 6325 4050
F 0 "#PWR023" H 6325 3900 50  0001 C CNN
F 1 "+3.3V" H 6340 4223 50  0000 C CNN
F 2 "" H 6325 4050 50  0001 C CNN
F 3 "" H 6325 4050 50  0001 C CNN
	1    6325 4050
	-1   0    0    1   
$EndComp
Wire Wire Line
	6325 4050 6325 3975
Wire Wire Line
	6325 3875 6225 3875
Wire Wire Line
	6225 3875 6225 3525
Wire Wire Line
	6225 3525 5075 3525
Wire Wire Line
	5075 4125 5075 4025
$Comp
L power:GND #PWR026
U 1 1 62C592F7
P 5425 4125
F 0 "#PWR026" H 5425 3875 50  0001 C CNN
F 1 "GND" H 5430 3952 50  0000 C CNN
F 2 "" H 5425 4125 50  0001 C CNN
F 3 "" H 5425 4125 50  0001 C CNN
	1    5425 4125
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C14
U 1 1 62C592FD
P 5250 4125
F 0 "C14" V 5021 4125 50  0000 C CNN
F 1 "0.1u" V 5112 4125 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 5250 4125 50  0001 C CNN
F 3 "~" H 5250 4125 50  0001 C CNN
	1    5250 4125
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5150 4125 5075 4125
Connection ~ 5075 4125
Wire Wire Line
	5350 4125 5425 4125
$Comp
L power:+3.3V #PWR028
U 1 1 62C59306
P 5075 4200
F 0 "#PWR028" H 5075 4050 50  0001 C CNN
F 1 "+3.3V" H 5090 4373 50  0000 C CNN
F 2 "" H 5075 4200 50  0001 C CNN
F 3 "" H 5075 4200 50  0001 C CNN
	1    5075 4200
	-1   0    0    1   
$EndComp
Wire Wire Line
	5075 4200 5075 4125
$Comp
L power:GND #PWR025
U 1 1 62C5BF1A
P 6025 4075
F 0 "#PWR025" H 6025 3825 50  0001 C CNN
F 1 "GND" H 6030 3902 50  0000 C CNN
F 2 "" H 6025 4075 50  0001 C CNN
F 3 "" H 6025 4075 50  0001 C CNN
	1    6025 4075
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C13
U 1 1 62C5BF20
P 5850 4075
F 0 "C13" V 6079 4075 50  0000 C CNN
F 1 "1u" V 5988 4075 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 5850 4075 50  0001 C CNN
F 3 "~" H 5850 4075 50  0001 C CNN
	1    5850 4075
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5750 4075 5675 4075
Connection ~ 5675 4075
Wire Wire Line
	5950 4075 6025 4075
$Comp
L power:+3.3V #PWR027
U 1 1 62C5BF29
P 5675 4150
F 0 "#PWR027" H 5675 4000 50  0001 C CNN
F 1 "+3.3V" H 5690 4323 50  0000 C CNN
F 2 "" H 5675 4150 50  0001 C CNN
F 3 "" H 5675 4150 50  0001 C CNN
	1    5675 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	5675 4150 5675 4075
Wire Wire Line
	5675 3925 5075 3925
Wire Wire Line
	5675 3925 5675 4075
$Comp
L power:GND #PWR020
U 1 1 62C60633
P 5425 3825
F 0 "#PWR020" H 5425 3575 50  0001 C CNN
F 1 "GND" H 5430 3652 50  0000 C CNN
F 2 "" H 5425 3825 50  0001 C CNN
F 3 "" H 5425 3825 50  0001 C CNN
	1    5425 3825
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C10
U 1 1 62C60639
P 5250 3825
F 0 "C10" V 5479 3825 50  0000 C CNN
F 1 "1u" V 5388 3825 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 5250 3825 50  0001 C CNN
F 3 "~" H 5250 3825 50  0001 C CNN
	1    5250 3825
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5150 3825 5100 3825
Wire Wire Line
	5350 3825 5425 3825
$Comp
L power:GND #PWR015
U 1 1 62C76E69
P 5475 3325
F 0 "#PWR015" H 5475 3075 50  0001 C CNN
F 1 "GND" H 5480 3152 50  0000 C CNN
F 2 "" H 5475 3325 50  0001 C CNN
F 3 "" H 5475 3325 50  0001 C CNN
	1    5475 3325
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C7
U 1 1 62C76E6F
P 5300 3325
F 0 "C7" V 5529 3325 50  0000 C CNN
F 1 "1u" V 5438 3325 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 5300 3325 50  0001 C CNN
F 3 "~" H 5300 3325 50  0001 C CNN
	1    5300 3325
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5400 3325 5475 3325
Wire Wire Line
	5200 3325 5100 3325
Wire Wire Line
	5100 3825 5100 3325
Connection ~ 5100 3825
Wire Wire Line
	5100 3825 5075 3825
Connection ~ 5100 3325
Wire Wire Line
	5100 3325 5075 3325
$Comp
L power:GND #PWR016
U 1 1 62C7E26E
P 1650 3425
F 0 "#PWR016" H 1650 3175 50  0001 C CNN
F 1 "GND" H 1655 3252 50  0000 C CNN
F 2 "" H 1650 3425 50  0001 C CNN
F 3 "" H 1650 3425 50  0001 C CNN
	1    1650 3425
	0    1    -1   0   
$EndComp
$Comp
L Device:C_Small C8
U 1 1 62C7E274
P 1825 3425
F 0 "C8" V 2054 3425 50  0000 C CNN
F 1 "1u" V 1963 3425 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 1825 3425 50  0001 C CNN
F 3 "~" H 1825 3425 50  0001 C CNN
	1    1825 3425
	0    1    -1   0   
$EndComp
Wire Wire Line
	1725 3425 1650 3425
Wire Wire Line
	2075 3425 1925 3425
Text Label 1950 3425 0    50   ~ 0
1.1V
Text Label 5100 3475 0    50   ~ 0
1.1V
$Comp
L Device:Crystal_Small Y1
U 1 1 62C85298
P 1025 2825
F 0 "Y1" V 900 2875 50  0000 L CNN
F 1 "12MHz" V 775 2750 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_HC49-SD_HandSoldering" H 1025 2825 50  0001 C CNN
F 3 "~" H 1025 2825 50  0001 C CNN
	1    1025 2825
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C4
U 1 1 62C85F35
P 825 2650
F 0 "C4" V 596 2650 50  0000 C CNN
F 1 "27p" V 687 2650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 825 2650 50  0001 C CNN
F 3 "~" H 825 2650 50  0001 C CNN
	1    825  2650
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 62C86FC2
P 825 3000
F 0 "C5" V 596 3000 50  0000 C CNN
F 1 "27p" V 687 3000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 825 3000 50  0001 C CNN
F 3 "~" H 825 3000 50  0001 C CNN
	1    825  3000
	0    1    1    0   
$EndComp
Wire Wire Line
	925  3000 1025 3000
Wire Wire Line
	1025 3000 1025 2925
Wire Wire Line
	1025 2725 1025 2650
Wire Wire Line
	1025 2650 925  2650
Wire Wire Line
	725  3000 625  3000
Wire Wire Line
	625  3000 625  2650
Wire Wire Line
	625  2650 725  2650
Wire Wire Line
	2075 3125 1475 3125
Wire Wire Line
	1475 3125 1475 2650
Wire Wire Line
	1475 2650 1025 2650
Connection ~ 1025 2650
$Comp
L Device:R_Small R8
U 1 1 62C93EA7
P 1250 3000
F 0 "R8" V 1054 3000 50  0000 C CNN
F 1 "1k" V 1145 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 1250 3000 50  0001 C CNN
F 3 "~" H 1250 3000 50  0001 C CNN
	1    1250 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	1150 3000 1025 3000
Connection ~ 1025 3000
Wire Wire Line
	1350 3000 1350 3225
Wire Wire Line
	1350 3225 2075 3225
Wire Wire Line
	6350 4625 6200 4625
Wire Wire Line
	6200 4625 6200 3625
Wire Wire Line
	6200 3625 5900 3625
Wire Wire Line
	6350 4725 6175 4725
Wire Wire Line
	6175 4725 6175 3725
Wire Wire Line
	6175 3725 5900 3725
$Comp
L Device:R_Small R4
U 1 1 62CC47FB
P 4125 1700
F 0 "R4" V 3929 1700 50  0000 C CNN
F 1 "2.2k" V 4020 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 4125 1700 50  0001 C CNN
F 3 "~" H 4125 1700 50  0001 C CNN
	1    4125 1700
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 62CC5D40
P 3550 1700
F 0 "R3" V 3354 1700 50  0000 C CNN
F 1 "2.2k" V 3445 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 3550 1700 50  0001 C CNN
F 3 "~" H 3550 1700 50  0001 C CNN
	1    3550 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	4025 1700 3875 1700
Wire Wire Line
	3650 1700 3775 1700
Wire Wire Line
	3450 1700 3450 1925
Wire Wire Line
	3450 1925 3275 1925
Wire Wire Line
	4225 1700 4225 2225
Connection ~ 4225 2225
Wire Wire Line
	4225 2225 4175 2225
$Comp
L power:GND #PWR011
U 1 1 62D01723
P 5200 2625
F 0 "#PWR011" H 5200 2375 50  0001 C CNN
F 1 "GND" H 5205 2452 50  0000 C CNN
F 2 "" H 5200 2625 50  0001 C CNN
F 3 "" H 5200 2625 50  0001 C CNN
	1    5200 2625
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5200 2625 5075 2625
$Comp
L power:+3.3V #PWR029
U 1 1 62C5EA1A
P 9625 4400
F 0 "#PWR029" H 9625 4250 50  0001 C CNN
F 1 "+3.3V" H 9640 4573 50  0000 C CNN
F 2 "" H 9625 4400 50  0001 C CNN
F 3 "" H 9625 4400 50  0001 C CNN
	1    9625 4400
	0    1    -1   0   
$EndComp
Wire Wire Line
	6350 4225 6350 4425
Wire Wire Line
	6650 5025 6700 5025
$Comp
L power:GND #PWR038
U 1 1 62C7C8CF
P 6700 5050
F 0 "#PWR038" H 6700 4800 50  0001 C CNN
F 1 "GND" H 6705 4877 50  0000 C CNN
F 2 "" H 6700 5050 50  0001 C CNN
F 3 "" H 6700 5050 50  0001 C CNN
	1    6700 5050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6700 5050 6700 5025
Connection ~ 6700 5025
Wire Wire Line
	6700 5025 6750 5025
$Comp
L Switch:SW_Push SW1
U 1 1 62DC6F52
P 1325 3725
F 0 "SW1" H 1325 4010 50  0000 C CNN
F 1 "RST" H 1325 3919 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_CK_KMR2" H 1325 3925 50  0001 C CNN
F 3 "~" H 1325 3925 50  0001 C CNN
	1    1325 3725
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C12
U 1 1 62DC86A0
P 1325 4000
F 0 "C12" V 1096 4000 50  0000 C CNN
F 1 "0.1u" V 1187 4000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 1325 4000 50  0001 C CNN
F 3 "~" H 1325 4000 50  0001 C CNN
	1    1325 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	1425 4000 1525 4000
Wire Wire Line
	1525 4000 1525 3725
Wire Wire Line
	1125 3725 1125 4000
Wire Wire Line
	1125 4000 1225 4000
Wire Wire Line
	2075 3725 1525 3725
Connection ~ 1525 3725
$Comp
L power:GND #PWR022
U 1 1 62DF8989
P 1125 4050
F 0 "#PWR022" H 1125 3800 50  0001 C CNN
F 1 "GND" H 1130 3877 50  0000 C CNN
F 2 "" H 1125 4050 50  0001 C CNN
F 3 "" H 1125 4050 50  0001 C CNN
	1    1125 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1125 4050 1125 4000
Connection ~ 1125 4000
Connection ~ 7625 4400
Wire Wire Line
	7625 4400 7425 4400
Wire Wire Line
	8325 4800 8325 4875
$Comp
L power:GND #PWR036
U 1 1 6320EAE7
P 8325 4875
F 0 "#PWR036" H 8325 4625 50  0001 C CNN
F 1 "GND" H 8330 4702 50  0000 C CNN
F 2 "" H 8325 4875 50  0001 C CNN
F 3 "" H 8325 4875 50  0001 C CNN
	1    8325 4875
	1    0    0    -1  
$EndComp
Connection ~ 7850 4400
Wire Wire Line
	7625 4400 7625 4450
Wire Wire Line
	7850 4400 7625 4400
Connection ~ 8925 4400
Wire Wire Line
	8925 4400 9250 4400
Wire Wire Line
	8725 4400 8925 4400
Wire Wire Line
	8925 4600 8925 4650
$Comp
L Device:C_Small C16
U 1 1 6320EADA
P 8925 4500
F 0 "C16" H 9017 4546 50  0000 L CNN
F 1 "1u" H 9017 4455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 8925 4500 50  0001 C CNN
F 3 "~" H 8925 4500 50  0001 C CNN
	1    8925 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR033
U 1 1 6320EAD4
P 8925 4675
F 0 "#PWR033" H 8925 4425 50  0001 C CNN
F 1 "GND" H 8930 4502 50  0000 C CNN
F 2 "" H 8925 4675 50  0001 C CNN
F 3 "" H 8925 4675 50  0001 C CNN
	1    8925 4675
	1    0    0    -1  
$EndComp
Wire Wire Line
	7625 4650 7625 4725
$Comp
L Device:C_Small C17
U 1 1 6320EACD
P 7625 4550
F 0 "C17" H 7717 4596 50  0000 L CNN
F 1 "1u" H 7717 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 7625 4550 50  0001 C CNN
F 3 "~" H 7625 4550 50  0001 C CNN
	1    7625 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR034
U 1 1 6320EAC7
P 7625 4725
F 0 "#PWR034" H 7625 4475 50  0001 C CNN
F 1 "GND" H 7630 4552 50  0000 C CNN
F 2 "" H 7625 4725 50  0001 C CNN
F 3 "" H 7625 4725 50  0001 C CNN
	1    7625 4725
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 4400 7925 4400
Wire Wire Line
	7850 4600 7850 4400
Wire Wire Line
	7925 4600 7850 4600
$Comp
L Regulator_Linear:ADP7142AUJZ-5.0 U2
U 1 1 6320EABE
P 8325 4500
F 0 "U2" H 8325 4867 50  0000 C CNN
F 1 "ADP7142AUJZ-5.0" H 8325 4776 50  0000 C CNN
F 2 "Package_SO:SC-74-6_1.5x2.9mm_P0.95mm" H 8325 4100 50  0001 C CIN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADP7142.pdf" H 8325 4000 50  0001 C CNN
	1    8325 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7425 4400 7425 4225
Wire Wire Line
	7425 4225 6350 4225
$Comp
L Device:R_Small R12
U 1 1 6332AFE7
P 1625 5000
F 0 "R12" V 1429 5000 50  0000 C CNN
F 1 "2.2k" V 1520 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 1625 5000 50  0001 C CNN
F 3 "~" H 1625 5000 50  0001 C CNN
	1    1625 5000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R13
U 1 1 6332AFED
P 2200 5000
F 0 "R13" V 2004 5000 50  0000 C CNN
F 1 "2.2k" V 2095 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2200 5000 50  0001 C CNN
F 3 "~" H 2200 5000 50  0001 C CNN
	1    2200 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1725 5000 1875 5000
Wire Wire Line
	2100 5000 1975 5000
Wire Wire Line
	2300 5000 2300 4775
Wire Wire Line
	1525 5000 1525 4775
Wire Wire Line
	1975 3925 2075 3925
Wire Wire Line
	1875 3825 2075 3825
Wire Wire Line
	2300 4775 1525 4775
Connection ~ 1525 4775
Wire Wire Line
	1525 4775 1525 4475
$Comp
L power:+3.3V #PWR031
U 1 1 63351BEA
P 1525 4475
F 0 "#PWR031" H 1525 4325 50  0001 C CNN
F 1 "+3.3V" H 1540 4648 50  0000 C CNN
F 2 "" H 1525 4475 50  0001 C CNN
F 3 "" H 1525 4475 50  0001 C CNN
	1    1525 4475
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 62C61FD9
P 3200 800
F 0 "J1" V 3164 512 50  0000 R CNN
F 1 "input" V 3073 512 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 3200 800 50  0001 C CNN
F 3 "~" H 3200 800 50  0001 C CNN
	1    3200 800 
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 62C63462
P 3825 800
F 0 "J2" V 3789 512 50  0000 R CNN
F 1 "input" V 3698 512 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 3825 800 50  0001 C CNN
F 3 "~" H 3825 800 50  0001 C CNN
	1    3825 800 
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J3
U 1 1 62C69D74
P 4450 800
F 0 "J3" V 4414 512 50  0000 R CNN
F 1 "input" V 4323 512 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 4450 800 50  0001 C CNN
F 3 "~" H 4450 800 50  0001 C CNN
	1    4450 800 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3775 1700 3775 1200
Wire Wire Line
	3775 1200 3300 1200
Wire Wire Line
	3300 1200 3300 1000
Wire Wire Line
	3925 1000 3925 1200
Wire Wire Line
	3925 1200 3775 1200
Connection ~ 3775 1200
Wire Wire Line
	4550 1000 4550 1200
Wire Wire Line
	4550 1200 3925 1200
Connection ~ 3925 1200
Wire Wire Line
	3875 1275 4450 1275
Wire Wire Line
	4450 1275 4450 1000
Wire Wire Line
	3875 1275 3875 1700
Wire Wire Line
	3875 1275 3825 1275
Wire Wire Line
	3200 1275 3200 1000
Connection ~ 3875 1275
Wire Wire Line
	3825 1000 3825 1275
Connection ~ 3825 1275
Wire Wire Line
	3825 1275 3200 1275
Wire Wire Line
	3100 1000 3100 1350
Wire Wire Line
	3100 1350 3725 1350
Wire Wire Line
	4350 1350 4350 1000
Wire Wire Line
	3725 1000 3725 1350
Connection ~ 3725 1350
Wire Wire Line
	3725 1350 4350 1350
Text Label 7150 4225 0    50   ~ 0
V_bus
Text Label 4150 1350 0    50   ~ 0
V_bus
Wire Wire Line
	3400 1000 3400 1100
Wire Wire Line
	3400 1100 4025 1100
Wire Wire Line
	4650 1100 4650 1000
Wire Wire Line
	4025 1000 4025 1100
Connection ~ 4025 1100
Wire Wire Line
	4025 1100 4650 1100
$Comp
L power:GND #PWR01
U 1 1 62CE4F36
P 4700 1100
F 0 "#PWR01" H 4700 850 50  0001 C CNN
F 1 "GND" H 4705 927 50  0000 C CNN
F 2 "" H 4700 1100 50  0001 C CNN
F 3 "" H 4700 1100 50  0001 C CNN
	1    4700 1100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4700 1100 4650 1100
Connection ~ 4650 1100
$Comp
L Connector_Generic:Conn_01x04 J6
U 1 1 62D02FAB
P 2675 5875
F 0 "J6" V 2547 6055 50  0000 L CNN
F 1 "output" V 2638 6055 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 2675 5875 50  0001 C CNN
F 3 "~" H 2675 5875 50  0001 C CNN
	1    2675 5875
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 62D02FB1
P 2050 5875
F 0 "J5" V 1922 6055 50  0000 L CNN
F 1 "output" V 2013 6055 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 2050 5875 50  0001 C CNN
F 3 "~" H 2050 5875 50  0001 C CNN
	1    2050 5875
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J4
U 1 1 62D02FB7
P 1425 5875
F 0 "J4" V 1297 6055 50  0000 L CNN
F 1 "output" V 1388 6055 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 1425 5875 50  0001 C CNN
F 3 "~" H 1425 5875 50  0001 C CNN
	1    1425 5875
	0    1    1    0   
$EndComp
Wire Wire Line
	1425 5675 1425 5300
Wire Wire Line
	2675 5300 2675 5675
Wire Wire Line
	2050 5675 2050 5300
Connection ~ 2050 5300
Wire Wire Line
	2050 5300 2675 5300
Wire Wire Line
	1325 5675 1325 5375
Wire Wire Line
	1325 5375 1875 5375
Wire Wire Line
	2575 5375 2575 5675
Wire Wire Line
	1950 5675 1950 5375
Connection ~ 1950 5375
Wire Wire Line
	1950 5375 2575 5375
Wire Wire Line
	1425 5300 1975 5300
Wire Wire Line
	1975 5000 1975 5300
Connection ~ 1975 5300
Wire Wire Line
	1975 5300 2050 5300
Wire Wire Line
	1875 5000 1875 5375
Connection ~ 1875 5375
Wire Wire Line
	1875 5375 1950 5375
Wire Wire Line
	1225 5675 1225 5475
Wire Wire Line
	1225 5475 1850 5475
Wire Wire Line
	2475 5475 2475 5675
Wire Wire Line
	1850 5675 1850 5475
Connection ~ 1850 5475
Wire Wire Line
	1850 5475 2475 5475
Text Label 2250 5475 0    50   ~ 0
V_bus
Wire Wire Line
	2775 5675 2775 5575
Wire Wire Line
	2775 5575 2150 5575
Wire Wire Line
	1525 5575 1525 5675
Wire Wire Line
	2150 5675 2150 5575
Connection ~ 2150 5575
Wire Wire Line
	2150 5575 1525 5575
$Comp
L power:GND #PWR040
U 1 1 62D8FE11
P 2800 5575
F 0 "#PWR040" H 2800 5325 50  0001 C CNN
F 1 "GND" H 2805 5402 50  0000 C CNN
F 2 "" H 2800 5575 50  0001 C CNN
F 3 "" H 2800 5575 50  0001 C CNN
	1    2800 5575
	0    -1   1    0   
$EndComp
Wire Wire Line
	2800 5575 2775 5575
Connection ~ 2775 5575
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 62DEE2BF
P 3875 4775
AR Path="/631F8354/62DEE2BF" Ref="J?"  Part="1" 
AR Path="/62DEE2BF" Ref="J22"  Part="1" 
F 0 "J22" H 3955 4817 50  0000 L CNN
F 1 "ext" H 3955 4726 50  0000 L CNN
F 2 "Connector:FanPinHeader_1x03_P2.54mm_Vertical" H 3875 4775 50  0001 C CNN
F 3 "~" H 3875 4775 50  0001 C CNN
	1    3875 4775
	0    1    1    0   
$EndComp
Wire Wire Line
	3775 4575 3775 4325
Wire Wire Line
	3875 4575 3875 4325
Wire Wire Line
	3975 4575 3975 4325
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 62E2936B
P 1775 2725
AR Path="/631F8354/62E2936B" Ref="J?"  Part="1" 
AR Path="/62E2936B" Ref="J19"  Part="1" 
F 0 "J19" H 1855 2767 50  0000 L CNN
F 1 "I2C" H 1855 2676 50  0000 L CNN
F 2 "Connector:FanPinHeader_1x03_P2.54mm_Vertical" H 1775 2725 50  0001 C CNN
F 3 "~" H 1775 2725 50  0001 C CNN
	1    1775 2725
	-1   0    0    1   
$EndComp
Wire Wire Line
	1975 2625 2075 2625
Wire Wire Line
	2075 2725 1975 2725
Wire Wire Line
	2075 2825 1975 2825
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 62E6447C
P 9450 4550
AR Path="/631F8354/62E6447C" Ref="J?"  Part="1" 
AR Path="/62E6447C" Ref="J21"  Part="1" 
F 0 "J21" H 9530 4542 50  0000 L CNN
F 1 "power" H 9530 4451 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-2_1x02_P5.00mm_Horizontal" H 9450 4550 50  0001 C CNN
F 3 "~" H 9450 4550 50  0001 C CNN
	1    9450 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 4550 9250 4400
Connection ~ 9250 4400
Wire Wire Line
	9250 4400 9450 4400
Wire Wire Line
	9250 4650 8925 4650
Connection ~ 8925 4650
Wire Wire Line
	8925 4650 8925 4675
$Comp
L Device:LED_Small D?
U 1 1 62E928AF
P 10100 3950
AR Path="/631F8354/62E928AF" Ref="D?"  Part="1" 
AR Path="/62E928AF" Ref="D4"  Part="1" 
F 0 "D4" H 10100 3743 50  0000 C CNN
F 1 "LED_Small" H 10100 3834 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10100 3950 50  0001 C CNN
F 3 "~" V 10100 3950 50  0001 C CNN
	1    10100 3950
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 62E928B5
P 9725 3950
AR Path="/631F8354/62E928B5" Ref="R?"  Part="1" 
AR Path="/62E928B5" Ref="R11"  Part="1" 
F 0 "R11" H 9784 3996 50  0000 L CNN
F 1 "2.2k" H 9784 3905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 9725 3950 50  0001 C CNN
F 3 "~" H 9725 3950 50  0001 C CNN
	1    9725 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10000 3950 9825 3950
Wire Wire Line
	9625 3950 9450 3950
$Comp
L power:GND #PWR?
U 1 1 62E928BD
P 10300 4050
AR Path="/631F8354/62E928BD" Ref="#PWR?"  Part="1" 
AR Path="/62E928BD" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 10300 3800 50  0001 C CNN
F 1 "GND" H 10305 3877 50  0000 C CNN
F 2 "" H 10300 4050 50  0001 C CNN
F 3 "" H 10300 4050 50  0001 C CNN
	1    10300 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 4050 10300 3950
Wire Wire Line
	10300 3950 10200 3950
Wire Wire Line
	9450 3950 9450 4400
Connection ~ 9450 4400
Wire Wire Line
	9450 4400 9625 4400
Connection ~ 1875 5000
Wire Wire Line
	1875 3825 1875 5000
Connection ~ 1975 5000
Wire Wire Line
	1975 3925 1975 5000
Text Label 3775 1700 3    50   ~ 0
SCL
Text Label 3875 1700 3    50   ~ 0
SDA
Text Label 2875 4325 3    50   ~ 0
SCL
Text Label 2975 4325 3    50   ~ 0
SDA
$Comp
L power:GND #PWR?
U 1 1 631613C1
P 625 3050
AR Path="/631F8354/631613C1" Ref="#PWR?"  Part="1" 
AR Path="/631613C1" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 625 2800 50  0001 C CNN
F 1 "GND" H 630 2877 50  0000 C CNN
F 2 "" H 625 3050 50  0001 C CNN
F 3 "" H 625 3050 50  0001 C CNN
	1    625  3050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	625  3050 625  3000
Connection ~ 625  3000
$Comp
L Connector:USB_B_Micro J8
U 1 1 62B2DF51
P 6650 4625
F 0 "J8" H 6420 4614 50  0000 R CNN
F 1 "USB_B_Micro" H 6420 4523 50  0000 R CNN
F 2 "Connector_USB:USB_Micro-B_GCT_USB3076-30-A" H 6800 4575 50  0001 C CNN
F 3 "~" H 6800 4575 50  0001 C CNN
	1    6650 4625
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
